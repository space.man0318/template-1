import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'
import Home from 'components/home/Home'
import './App.scss'

const useStyles = makeStyles((theme) => ({
    app: {},
}))

function App() {
    const classes = useStyles()

    return (
        <div className={classes.app}>
            <>
                <Switch>
                    <Route exact path="/" component={Home} />
                </Switch>
            </>
        </div>
    )
}

export default App
