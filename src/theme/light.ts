import { ThemeOptions } from '@material-ui/core'
import { PaletteOptions } from '@material-ui/core/styles/createPalette'
import typography from './typography'
import palette from './lightPalette'

// A custom theme for this app
const theme: ThemeOptions = {
    palette: palette as PaletteOptions,
    typography,
}

export default theme
