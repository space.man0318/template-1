import { TypographyOptions } from '@material-ui/core/styles/createTypography'

const fontFamily = [
    '"Gilroy"',
    '-apple-system',
    'BlinkMacSystemFont',
    'sans-serif',
    '"Apple Color Emoji"',
    '"Segoe UI Emoji"',
    '"Segoe UI Symbol"',
].join(',')

const bodyFamily = [
    '"Montserrat"',
    '-apple-system',
    'BlinkMacSystemFont',
    'sans-serif',
    '"Apple Color Emoji"',
    '"Segoe UI Emoji"',
    '"Segoe UI Symbol"',
].join(',')

const typography: TypographyOptions = {
    fontFamily,
    htmlFontSize: 10,
    fontSize: 12,
    h1: {
        fontFamily,
        fontWeight: 900,
        fontSize: '7.2rem',
        lineHeight: '8rem',
    },
    h2: {
        fontFamily,
        fontWeight: 900,
        fontSize: '6rem',
        lineHeight: '7.2rem',
    },
    h3: {
        fontFamily,
        fontWeight: 900,
        fontSize: '3.2rem',
        lineHeight: '4rem',
    },
    h4: {
        fontFamily,
        fontWeight: 900,
        fontSize: '2.8rem',
        lineHeight: '3.2rem',
    },
    h5: {
        fontFamily,
        fontWeight: 700,
        fontSize: '2.4rem',
        lineHeight: '2.8rem',
    },
    h6: {
        fontFamily,
        fontWeight: 700,
        fontSize: '2rem',
        lineHeight: '3.2rem',
    },
    subtitle1: {
        fontFamily,
        fontWeight: 700,
        fontSize: '2.4rem',
        lineHeight: '2.8rem',
    },
    subtitle2: {
        fontFamily,
        fontWeight: 700,
        fontSize: '1.8rem',
        lineHeight: '2.4rem',
    },
    body1: {
        fontFamily: bodyFamily,
        fontWeight: 400,
        fontSize: '1.6rem',
        lineHeight: '2.4rem',
    },
    body2: {
        fontFamily: bodyFamily,
        fontWeight: 400,
        fontSize: '1.2rem',
        lineHeight: '2rem',
    },
    button: {
        fontFamily: bodyFamily,
        fontWeight: 700,
        fontSize: '1.4rem',
        lineHeight: '1.6rem',
    },
    caption: {
        fontFamily: bodyFamily,
        fontWeight: 700,
        fontSize: '1.2rem',
        lineHeight: '1.6rem',
        letterSpacing: 0.25,
    },
    overline: {
        fontFamily: bodyFamily,
        fontWeight: 400,
        fontSize: '1.2rem',
        lineHeight: '2.4rem',
    },
}

export default typography
