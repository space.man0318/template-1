// import { PaletteOptions } from '@material-ui/core/styles/createPalette'

const palette = {
    type: 'light',
    divider: '#D4D3D9',
    primary: {
        light: '#67A6F4',
        main: '#4392F1',
        dark: '#1A7AEF',
    },
    secondary: {
        light: '#5D6A98',
        main: '#414A6B',
        dark: '#363E59',
    },
    error: {
        light: '#EBA79B',
        main: '#E0786C',
        dark: '#D45C55',
    },
    warning: {
        light: '#EBC491',
        main: '#EDAB66',
        dark: '#D48744',
    },
    info: {
        light: '#9BEBD0',
        main: '#69DBBB',
        dark: '#53D0AB',
    },
    success: {
        light: '#9BE698',
        main: '#71DB69',
        dark: '#5FD053',
    },
    action: {
        active: '#A7A7B8',
        hover: '#595766',
        selected: '#585766',
        disabled: '#1F1F26',
        focus: '#505059',
    },
    common: {
        black: '#1C1B20',
        white: '#FFFFFF',
    },
    background: {
        paper: '#F5F5F5',
        default: '#F8F8F8',
    },
    text: {
        primary: '#1C1B20',
        secondary: '#babcc1',
        disabled: '#4C4A57',
        hint: '#6CE098',
    },
    grey: {
        '50': '#fcfcfd',
        '100': '#f3f3f6',
        '200': '#e9e9ef',
        '300': '#d6d6e0',
        '400': '#afafc3',
        '500': '#9b9bb4',
        '600': '#757597',
        '700': '#646486',
        '800': '#47475f',
        '900': '#2A2A38',
        A100: '#dfdfe7',
        A200: '#a5a5bc',
        A400: '#313142',
        A700: '#6c6c8f',
    },
}

export default palette
