import React from 'react'
import Link, { LinkProps } from '@material-ui/core/Link'
import { makeStyles } from '@material-ui/core/styles'

const fontFamily = [
    '"Libre Baskerville"',
    'serif',
    '"Apple Color Emoji"',
    '"Segoe UI Emoji"',
    '"Segoe UI Symbol"',
]

const useStyles = makeStyles((theme) => ({
    root: {
        position: 'relative',
        color: theme.palette.text.primary,
        fontSize: 16,
        zIndex: 1,
        '&:after, &:before': {
            content: '""',
            position: 'absolute',
            overflow: 'hidden',
            transition: 'transform 400ms cubic-bezier(1, 0, 0, 1) 0ms',
            zIndex: 0,
        },
        '&:before': {
            top: 5,
            bottom: 7,
            height: 'auto',
            opacity: 0.5,
            left: -5,
            right: -5,
            transform: 'scaleX(0)',
            transformOrigin: 'bottom right',
            background: theme.palette.primary.light,
        },
        '&:after': {
            bottom: 0,
            left: 0,
            right: 0,
            height: 2,
            transformOrigin: 'bottom left',
            backgroundColor: 'currentColor',
        },
        '&:hover:before': {
            transform: 'scaleX(1)',
            transformOrigin: 'bottom left',
        },
        '&:hover:after': {
            transform: 'scaleX(0)',
            transformOrigin: 'bottom right',
        },
        fontFamily: fontFamily.join(', '),
    },
}))

export default function StyledLink(props: LinkProps) {
    const classes = useStyles(props)

    return (
        <Link
            classes={{
                root: classes.root,
            }}
            underline="none"
            {...props}
        />
    )
}
