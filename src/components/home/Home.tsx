import React, { useEffect, useState } from 'react'
import _ from 'lodash'
import clsx from 'clsx'
import { useBoundingclientrectRef } from 'rooks'
import Splitting from 'splitting'
import { gsap } from 'gsap'
import { ScrollTrigger } from 'gsap/ScrollTrigger'
import { makeStyles, useTheme } from '@material-ui/core/styles'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import Box from '@material-ui/core/Box'
import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import Link from 'components/Link'
// icon
import ArrowTopLeftIcon from 'mdi-react/ArrowTopLeftIcon'
import InstagramIcon from 'mdi-react/InstagramIcon'
import TwitterIcon from 'mdi-react/TwitterIcon'
import FacebookIcon from 'mdi-react/FacebookIcon'
import WaveIcon from 'components/icons/WaveIcon'
import titleImg from 'assets/images/title.svg'
import logo from 'assets/images/logo.png'
import image1 from 'assets/images/image1.jpg'
import image2 from 'assets/images/image2.jpg'
import image3 from 'assets/images/image3.jpg'

// 編輯 - 連結
const links = {
    facebook: '#',
    instagram: '#',
    twitter: '#',
}

gsap.registerPlugin(ScrollTrigger)

const useStyles = makeStyles((theme) => ({
    root: {
        position: 'relative',
    },
    content: {
        position: 'relative',
        zIndex: 1,
        backgroundColor: theme.palette.background.default,
    },
    navWrap: {
        position: 'absolute',
        top: 0,
        width: '100%',
        zIndex: theme.zIndex.appBar,
    },
    logo: {
        position: 'absolute',
        height: 32,
    },
    wrap1: {
        position: 'relative',
        width: '100vw',
    },
    div1: {
        width: '50%',
        height: '100vh',
        textAlign: 'center',
    },
    title: {
        position: 'absolute',
        width: '100vw',
        height: '100vh',
    },
    scroll: {
        display: 'inline-block',
        opacity: 0,
        transform: 'rotate(-90deg) translateY(32px)',
        transition: 'opacity 250ms ease-in 2000ms',
    },
    arrow: {
        display: 'inline-block',
        animation:
            'bounce 800ms cubic-bezier(0.7, 0, 0.3, 1) infinite alternate',
    },
    arrowIcon: {
        width: 12,
        transform: 'rotate(-45deg) translate(-20px, -8px)',
    },
    dateDiv: {
        position: 'relative',
        display: 'inline-block',
        zIndex: 1,
        backgroundColor: theme.palette.primary.main,
        // transform: 'translate(-32px, 0)',
    },
    bg1: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '50%',
        height: '100%',
        backgroundColor: theme.palette.primary.main,
        zIndex: -1,
    },
    bg2: {
        width: '100%',
        height: '100%',
        backgroundColor: theme.palette.common.black,
        zIndex: -1,
    },
    image: {
        width: '100%',
        objectFit: 'cover',
    },
    linkSmall: {
        fontSize: 12,
        '&:before': {
            top: 5,
            bottom: 5,
        },
        '&:after': {
            height: 1,
            bottom: -2,
        },
    },
    borderDiv: {
        position: 'fixed',
        width: '100vw',
        height: '100vh',
        pointerEvents: 'none',
        zIndex: theme.zIndex.appBar,
        transition: 'border 500ms ease-in-out',
        border: 'white solid 0px',
    },
    border: {
        border: 'white solid 40px',
    },
    rooter: {
        position: 'fixed',
        width: '100%',
        bottom: 0,
        left: 0,
        zIndex: 0,
    },
    wave: {
        display: 'inline-block',
        overflow: 'hidden',
    },
}))

function Home() {
    const classes = useStyles()
    const theme = useTheme()
    const matchSm = useMediaQuery(theme.breakpoints.up('sm'))
    const matchMd = useMediaQuery(theme.breakpoints.up('md'))
    const [rooterRef, rooterRect] = useBoundingclientrectRef()

    const [mounted, setMounted] = useState(false)

    useEffect(() => {
        setMounted(true)
    }, [mounted])

    useEffect(() => {
        if (!mounted) {
            return
        }

        // wrap1 animation
        wrapLines('.wrap1-text', 'wrap1-line', 'Track1')
        const tl = gsap.timeline()

        tl.to(
            '#title',
            { x: matchSm ? '100%' : '50%', duration: 1, ease: 'circ' },
            0
        )
        tl.to(
            '#bg1',
            {
                transformOrigin: 'left',
                scaleX: 2,
                duration: 1,
                ease: 'circ',
            },
            0
        )

        tl.from(
            '.wrap1-dateDiv',
            {
                x: -64,
                duration: 0.5,
            },
            0.6
        )

        tl.from(
            '.wrap1-date',
            {
                opacity: 0,
                duration: 0.5,
                ease: 'circ',
            },
            0.65
        )

        tl.from(
            '.wrap1-line',
            {
                y: '100%',
                ease: 'circ',
                duration: 0.5,
                stagger: {
                    each: 0.1,
                },
            },
            1
        )

        if (matchSm) {
            tl.set('.wrap1-content', { opacity: 0 }, 0)

            gsap.to('.wrap1-intro', {
                scrollTrigger: {
                    endTrigger: '#wrap1',
                    end: 'bottom top',
                    scrub: true,
                },
                y: (i, target) => {
                    const parentRect = target.parentNode.getBoundingClientRect()
                    const parentHeight = _.get(parentRect, 'height')
                    if (parentHeight) {
                        return parentHeight * _.get(target, 'dataset.speed', 1)
                    }
                    return 0
                },
                ease: 'none',
            })
        } else {
            tl.to(
                '#scroll',
                {
                    y: 50,
                    opacity: 0,
                    duration: 0.5,
                    ease: 'circ',
                },
                0.25
            )
        }

        tl.progress(1)
        tl.reverse()

        tl.set('.wrap1-content', { opacity: 1 }, 0)

        const scrollTrigger = ScrollTrigger.create({
            trigger: '#wrap1',
            start: 'top top-=20px',
            end: 'top top-=21px',
            scrub: true,
            onEnter: ({ progress, direction, isActive }) => {
                tl.play()
            },
            onEnterBack: ({ progress, direction, isActive }) => {
                tl.reverse()
            },
        })

        return () => {
            tl.set('.wrap1-dateDiv', { x: 0 }, 0)
            tl.set('.wrap1-date', { opacity: 1 }, 0)
            tl.pause()
            tl.progress(0)
            tl.clear(true)
            scrollTrigger.kill()
        }
    }, [matchSm, mounted])

    useEffect(() => {
        if (!mounted) {
            return
        }
        // wrap2 animation
        const wrappers = ['wrap2', 'wrap3', 'wrap4']

        wrappers.forEach((el) => {
            const tl = gsap.timeline()

            tl.from(
                `.${el}-wave`,
                {
                    transformOrigin: 'left',
                    width: 0,
                    duration: 1,
                },
                0.25
            )

            tl.from(
                `.${el}-line`,
                {
                    y: '100%',
                    ease: 'circ',
                    duration: 1,
                    stagger: {
                        each: 0.25,
                    },
                },
                0.25
            )

            tl.from(
                `.${el}-text`,
                {
                    opacity: 0,
                    duration: 1,
                    stagger: {
                        each: 0.5,
                    },
                },
                1.5
            )

            tl.pause()
            tl.progress(0)

            ScrollTrigger.create({
                trigger: `#${el}`,
                start: 'top 75%',
                end: 'bottom center',
                scrub: true,
                onEnter: ({ isActive }) => {
                    if (isActive) {
                        tl.play()
                    }
                },
                onEnterBack: ({ isActive }) => {
                    if (isActive) {
                        tl.play()
                    }
                },
            })
        })
    }, [mounted])

    useEffect(() => {
        // parallax
        const parallaxList = document.querySelectorAll('.parallax')
        if (mounted) {
            _.forEach(parallaxList, (e, index) => {
                const trigger = `.parallax-${index}`
                gsap.to(trigger, {
                    scrollTrigger: {
                        trigger,
                        scrub: true,
                    },
                    y: (i, target) => {
                        if (target) {
                            const targetRect = target.getBoundingClientRect()
                            const parentRect = target.parentNode.getBoundingClientRect()

                            const targetHeight = _.get(targetRect, 'height')
                            const parentHeight = _.get(parentRect, 'height')

                            if (targetHeight && parentHeight) {
                                const diff = Math.abs(
                                    targetHeight - parentHeight
                                )
                                return -diff * _.get(target, 'dataset.speed', 1)
                            }
                        }
                        return 0
                    },
                    ease: 'none',
                })
            })
        }
    }, [mounted])

    const renderLinks = (size: number = 16) => {
        return (
            <>
                <a
                    className="d-inline-block"
                    href={links.facebook}
                    rel="noreferrer noopener"
                    target="_blank"
                    style={{ marginBottom: 12, color: 'inherit' }}
                >
                    <IconButton
                        color="inherit"
                        style={{ marginRight: size / 2 }}
                        size="small"
                    >
                        <FacebookIcon size={size} />
                    </IconButton>
                </a>
                <a
                    className="d-inline-block"
                    href={links.instagram}
                    rel="noreferrer noopener"
                    target="_blank"
                    style={{ marginBottom: 12, color: 'inherit' }}
                >
                    <IconButton
                        color="inherit"
                        style={{ marginRight: size / 2 }}
                        size="small"
                    >
                        <InstagramIcon size={size} />
                    </IconButton>
                </a>
                <a
                    className="d-inline-block"
                    href={links.twitter}
                    rel="noreferrer noopener"
                    target="_blank"
                    style={{ marginBottom: 12, color: 'inherit' }}
                >
                    <IconButton
                        color="inherit"
                        style={{ marginRight: size / 2 }}
                        size="small"
                    >
                        <TwitterIcon size={size} />
                    </IconButton>
                </a>
            </>
        )
    }

    return (
        <Container
            className={classes.root}
            disableGutters
            style={{ marginBottom: _.get(rooterRect, 'height') }}
            maxWidth={false}
        >
            <div className={classes.content}>
                <div
                    className={clsx(classes.borderDiv, {
                        [classes.border]: matchSm,
                    })}
                />
                <div className={classes.navWrap}>
                    {/*      Navigation      */}
                    <img
                        className={classes.logo}
                        src={logo}
                        alt=""
                        style={
                            matchSm
                                ? { position: 'fixed', top: 4, left: 40 }
                                : {
                                      top: 32,
                                      left: 24,
                                  }
                        }
                    />
                </div>
                {/*      Wrap 1      */}
                <div id="wrap1" className={classes.wrap1}>
                    <div id="bg1" className={classes.bg1} />
                    <div
                        className="wrap1-intro"
                        style={{ position: 'relative' }}
                        data-speed={0.9}
                    >
                        <Grid
                            container
                            justify="center"
                            alignContent="center"
                            className={classes.title}
                        >
                            <Grid id="title" item xs={12} sm={6}>
                                <img
                                    className={classes.image}
                                    src={titleImg}
                                    alt=""
                                />
                            </Grid>
                        </Grid>
                    </div>

                    <div className={classes.div1}>
                        <div
                            style={
                                matchSm
                                    ? {
                                          position: 'fixed',
                                          color: theme.palette.common.black,
                                          bottom: 48,
                                          left: 8,
                                          zIndex: theme.zIndex.tooltip,
                                      }
                                    : {
                                          position: 'fixed',
                                          color: theme.palette.common.white,
                                          bottom: 96,
                                          left: 64,
                                          zIndex: theme.zIndex.tooltip,
                                      }
                            }
                        >
                            <div id="scroll">
                                <span
                                    className={clsx(classes.scroll, {
                                        show: mounted,
                                    })}
                                >
                                    <Typography variant="body2" color="inherit">
                                        <span className={classes.arrow}>
                                            <ArrowTopLeftIcon
                                                className={classes.arrowIcon}
                                            />
                                        </span>
                                        Scroll
                                    </Typography>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div
                        className="wrap1-intro wrap1-content"
                        data-speed={0.8}
                        style={
                            matchSm ? { position: 'absolute', top: '40%' } : {}
                        }
                    >
                        <Box p={5} pb={8} pl={matchSm ? 15 : undefined}>
                            <Grid container>
                                <Grid item xs={12} sm={5} md={12}>
                                    <Box mb={3} position="relative" pl={3}>
                                        <Typography variant="caption">
                                            <WaveIcon
                                                className="absolute-vertical"
                                                style={{ left: 0 }}
                                            />
                                            <span
                                                className={clsx(
                                                    'wrap1-dateDiv',
                                                    classes.dateDiv
                                                )}
                                            >
                                                <i className="wrap1-date">
                                                    {/* 編輯 - 文字 */}
                                                    Since 2004
                                                </i>
                                            </span>
                                        </Typography>
                                    </Box>
                                    <Box
                                        maxWidth={matchSm ? 512 : 200}
                                        color={theme.palette.common.white}
                                    >
                                        <Typography
                                            className="wrap1-text"
                                            variant={
                                                matchSm ? 'h5' : 'subtitle2'
                                            }
                                            color="textPrimary"
                                        >
                                            {/* 編輯 - 文字 */}
                                            Wokine is a global digital agency
                                        </Typography>

                                        <Typography
                                            className="wrap1-text"
                                            variant={
                                                matchSm ? 'h5' : 'subtitle2'
                                            }
                                            color="inherit"
                                        >
                                            {/* 編輯 - 文字 */}
                                            and a startup studio, providing
                                            innovative,
                                        </Typography>
                                        <Typography
                                            className="wrap1-text"
                                            variant={
                                                matchSm ? 'h5' : 'subtitle2'
                                            }
                                            color="inherit"
                                        >
                                            {/* 編輯 - 文字 */}
                                            modern and aesthetic solutions.
                                        </Typography>
                                    </Box>
                                </Grid>
                            </Grid>
                        </Box>
                    </div>
                </div>
                <div
                    className="overflow-hidden"
                    style={{
                        height: 0,
                        width: '100%',
                        paddingTop: '48%',
                        position: 'relative',
                    }}
                >
                    <div
                        className="absolute-center"
                        style={{ width: '100%', height: '100%' }}
                    >
                        <img
                            className={clsx(
                                classes.image,
                                'parallax',
                                'parallax-0'
                            )}
                            src={image1}
                            alt=""
                            data-speed={1}
                            style={{ height: '120%' }}
                        />
                    </div>
                </div>
                {/*      Wrap 2      */}
                <div id="wrap2">
                    <Box px={5} py={8}>
                        <Grid container spacing={4} justify="center">
                            <Grid
                                item
                                xs={12}
                                sm={1}
                                md={1}
                                style={{ paddingTop: 0, paddingBottom: 0 }}
                            ></Grid>
                            <Grid item xs={12} sm={7} md={4}>
                                <Box
                                    position="relative"
                                    mb={4}
                                    color={theme.palette.primary.main}
                                >
                                    <div
                                        className={clsx(
                                            classes.wave,
                                            'wrap2-wave'
                                        )}
                                    >
                                        <WaveIcon width={48} />
                                    </div>
                                </Box>
                                <Box mb={5}>
                                    <div className="overflow-hidden">
                                        <Typography
                                            className="wrap2-line"
                                            variant={matchSm ? 'h1' : 'h2'}
                                            color="primary"
                                        >
                                            {/* 編輯 - 文字 */}
                                            DIG
                                        </Typography>
                                    </div>
                                    <div className="overflow-hidden">
                                        <Typography
                                            className="wrap2-line"
                                            variant={matchSm ? 'h1' : 'h2'}
                                            color="primary"
                                            style={{ marginTop: -16 }}
                                        >
                                            {/* 編輯 - 文字 */}
                                            ITAL.
                                        </Typography>
                                    </div>
                                </Box>

                                <Box mb={5}>
                                    <div className="overflow-hidden">
                                        <Typography
                                            className="wrap2-line"
                                            variant={
                                                matchSm ? 'h4' : 'subtitle1'
                                            }
                                        >
                                            {/* 編輯 - 文字 */}
                                            An <i>overview</i>
                                        </Typography>
                                    </div>

                                    <div className="overflow-hidden">
                                        <Typography
                                            className="wrap2-line"
                                            variant={
                                                matchSm ? 'h4' : 'subtitle1'
                                            }
                                        >
                                            {/* 編輯 - 文字 */}
                                            of your digital
                                        </Typography>
                                    </div>

                                    <div className="overflow-hidden">
                                        <Typography
                                            className="wrap2-line"
                                            variant={
                                                matchSm ? 'h4' : 'subtitle1'
                                            }
                                        >
                                            {/* 編輯 - 文字 */}
                                            strategy
                                            <span
                                                style={{
                                                    color:
                                                        theme.palette.primary
                                                            .main,
                                                }}
                                            >
                                                .
                                            </span>
                                        </Typography>
                                    </div>
                                </Box>
                                <div className="wrap2-text">
                                    <Box mb={3} color={theme.palette.grey[900]}>
                                        <Typography
                                            variant="body2"
                                            color="inherit"
                                        >
                                            {/* 編輯 - 文字 */}A key component
                                            of your transformation, your digital
                                            strategy requires special attention.
                                            We provide you with the support you
                                            need all during your project.
                                            Through regular workshops,
                                            brainstormings and idea sharing, we
                                            help grow your initial idea into a
                                            cutting-edge digital solution.
                                        </Typography>
                                    </Box>

                                    <Box mb={5}>
                                        {/* 編輯 - 文字 */}
                                        <Link>Our values</Link>
                                    </Box>
                                </div>
                            </Grid>
                            <Grid item xs={12} sm={10} md={7}>
                                <div
                                    className="parallax parallax-1"
                                    data-speed={1.1}
                                >
                                    <Box
                                        px={{ sm: 5 }}
                                        py={8}
                                        mt={{ md: 30 }}
                                        position="relative"
                                    >
                                        <div
                                            className={clsx(
                                                classes.bg2,
                                                'absolute-center'
                                            )}
                                            style={{
                                                width: matchMd
                                                    ? '100%'
                                                    : '100vw',
                                            }}
                                        />

                                        <Grid container spacing={4}>
                                            <Grid item xs={12} sm={6}>
                                                <Box
                                                    mb={4}
                                                    color={
                                                        theme.palette.common
                                                            .white
                                                    }
                                                >
                                                    <Box mb={2}>
                                                        <WaveIcon />
                                                        <Typography
                                                            variant="subtitle2"
                                                            color="inherit"
                                                        >
                                                            {/* 編輯 - 文字 */}
                                                            Design
                                                        </Typography>
                                                    </Box>
                                                    <Typography
                                                        variant="body2"
                                                        color="textSecondary"
                                                    >
                                                        {/* 編輯 - 文字 */}
                                                        Web design
                                                        <br />
                                                        Art direction
                                                        <br />
                                                        Logo & branding
                                                        <br />
                                                        UI & UX
                                                    </Typography>
                                                </Box>
                                            </Grid>
                                            <Grid item xs={12} sm={6}>
                                                <Box
                                                    mb={4}
                                                    color={
                                                        theme.palette.common
                                                            .white
                                                    }
                                                >
                                                    <Box mb={2}>
                                                        <WaveIcon />
                                                        <Typography
                                                            variant="subtitle2"
                                                            color="inherit"
                                                        >
                                                            {/* 編輯 - 文字 */}
                                                            Digital & Mobile
                                                        </Typography>
                                                    </Box>
                                                    <Typography
                                                        variant="body2"
                                                        color="textSecondary"
                                                    >
                                                        {/* 編輯 - 文字 */}
                                                        Websites <br />
                                                        Responsive design
                                                        <br />
                                                        Mobile & Tablet
                                                        <br />
                                                        Mobile apps (iOS,
                                                        Android)
                                                    </Typography>
                                                </Box>
                                            </Grid>
                                            <Grid item xs={12} sm={6}>
                                                <Box
                                                    mb={4}
                                                    color={
                                                        theme.palette.common
                                                            .white
                                                    }
                                                >
                                                    <Box mb={2}>
                                                        <WaveIcon />
                                                        <Typography
                                                            variant="subtitle2"
                                                            color="inherit"
                                                        >
                                                            {/* 編輯 - 文字 */}
                                                            Web Development
                                                        </Typography>
                                                    </Box>
                                                    <Typography
                                                        variant="body2"
                                                        color="textSecondary"
                                                    >
                                                        {/* 編輯 - 文字 */}
                                                        Front end & Back end
                                                        <br />
                                                        Interaction design
                                                        <br />
                                                        Magento, Wordpress
                                                        <br />
                                                        Symfony, Laravel
                                                    </Typography>
                                                </Box>
                                            </Grid>
                                            <Grid item xs={12} sm={6}>
                                                <Box
                                                    mb={4}
                                                    color={
                                                        theme.palette.common
                                                            .white
                                                    }
                                                >
                                                    <Box mb={2}>
                                                        <WaveIcon />
                                                        <Typography
                                                            variant="subtitle2"
                                                            color="inherit"
                                                        >
                                                            {/* 編輯 - 文字 */}
                                                            Social Media &
                                                            Webmarketing
                                                        </Typography>
                                                    </Box>
                                                    <Typography
                                                        variant="body2"
                                                        color="textSecondary"
                                                    >
                                                        {/* 編輯 - 文字 */}
                                                        Social Media Strategy
                                                        <br />
                                                        Community Management
                                                        <br />
                                                        Bloggers & influencers
                                                        <br />
                                                        Reporting, live events
                                                        <br />
                                                        Mailing & competition
                                                    </Typography>
                                                </Box>
                                            </Grid>
                                        </Grid>
                                    </Box>
                                </div>
                            </Grid>
                        </Grid>
                    </Box>
                </div>
                {/*      Wrap 3      */}
                <div id="wrap3">
                    <Box px={5} py={8} mt={{ md: -20 }} pt={0}>
                        <Grid container spacing={4}>
                            <Grid
                                item
                                xs={12}
                                sm={1}
                                style={{ paddingTop: 0, paddingBottom: 0 }}
                            ></Grid>
                            <Grid item xs={12} sm={5}>
                                <Box mb={4}>
                                    <div
                                        className={clsx(
                                            classes.wave,
                                            'wrap3-wave'
                                        )}
                                    >
                                        <WaveIcon width={48} />
                                    </div>
                                </Box>
                                <Box mb={5}>
                                    <div className="overflow-hidden">
                                        <Typography
                                            className="wrap3-line"
                                            variant={matchSm ? 'h1' : 'h2'}
                                            color="textPrimary"
                                        >
                                            {/* 編輯 - 文字 */}C
                                        </Typography>
                                    </div>

                                    <div className="overflow-hidden">
                                        <Typography
                                            className="wrap3-line"
                                            variant={matchSm ? 'h1' : 'h2'}
                                            color="textPrimary"
                                            style={{ marginTop: -16 }}
                                        >
                                            {/* 編輯 - 文字 */}
                                            REA.
                                        </Typography>
                                    </div>
                                </Box>
                            </Grid>

                            <Grid item xs={12} sm={5}>
                                <div
                                    className="parallax parallax-2"
                                    data-speed={1.75}
                                >
                                    <Box mb={5} mt={{ sm: 60 }}>
                                        <div className="overflow-hidden">
                                            <Typography
                                                className="wrap3-line"
                                                variant={
                                                    matchSm ? 'h4' : 'subtitle1'
                                                }
                                            >
                                                {/* 編輯 - 文字 */}
                                                <i>User-centered</i>
                                            </Typography>
                                        </div>
                                        <div className="overflow-hidden">
                                            <Typography
                                                className="wrap3-line"
                                                variant={
                                                    matchSm ? 'h4' : 'subtitle1'
                                                }
                                            >
                                                {/* 編輯 - 文字 */}
                                                creativity
                                                <span
                                                    style={{
                                                        color:
                                                            theme.palette
                                                                .primary.main,
                                                    }}
                                                >
                                                    .
                                                </span>
                                            </Typography>
                                        </div>
                                    </Box>
                                    <div className="wrap3-text">
                                        <Box
                                            mb={3}
                                            color={theme.palette.grey[900]}
                                            maxWidth={matchSm ? 256 : undefined}
                                        >
                                            <Typography
                                                variant="body2"
                                                color="inherit"
                                            >
                                                {/* 編輯 - 文字 */}
                                                Efficient and immersive user
                                                experience is the way to capture
                                                attention and deliver a clear
                                                message. This is why we believe,
                                                first and foremost, that
                                                usability serves design. And
                                                that all design must be elegant
                                                and innovative.
                                            </Typography>
                                        </Box>
                                        {/* 編輯 - 文字 */}
                                        <Link>See our work</Link>
                                    </div>

                                    <div
                                        className={clsx('hide', {
                                            hidden: !matchSm,
                                        })}
                                    >
                                        <div
                                            className="overflow-hidden"
                                            style={{
                                                height: 0,
                                                width: '100%',
                                                paddingTop: '98%',
                                                position: 'relative',
                                            }}
                                        />

                                        <Box px={5} py={3} textAlign="right">
                                            <Typography
                                                display="block"
                                                variant="caption"
                                                gutterBottom
                                            >
                                                placeholder
                                            </Typography>
                                            <Typography
                                                display="block"
                                                variant="caption"
                                                color="textSecondary"
                                            >
                                                <i>placeholder</i>
                                            </Typography>
                                            <br />
                                            <Link className={classes.linkSmall}>
                                                placeholder
                                            </Link>
                                        </Box>
                                    </div>
                                </div>
                            </Grid>
                            <Grid item xs={12} sm={1}></Grid>

                            <Grid
                                item
                                xs={12}
                                sm={5}
                                style={{ position: 'relative' }}
                            >
                                <div
                                    style={
                                        matchSm
                                            ? {
                                                  transform:
                                                      'translateY(-480px)',
                                                  width: '100%',
                                              }
                                            : {}
                                    }
                                >
                                    <div
                                        style={
                                            matchSm
                                                ? {
                                                      position: 'absolute',
                                                      transform:
                                                          'translateY(-100%)',
                                                      width: '100%',
                                                  }
                                                : {}
                                        }
                                    >
                                        <Box
                                            position="relative"
                                            zIndex={-1}
                                            mx={-5}
                                        >
                                            <div
                                                className="overflow-hidden"
                                                style={{
                                                    height: 0,
                                                    width: '100%',
                                                    paddingTop: '98%',
                                                    position: 'relative',
                                                }}
                                            >
                                                <div
                                                    className="absolute-center"
                                                    style={{
                                                        width: '100%',
                                                        height: '100%',
                                                    }}
                                                >
                                                    <img
                                                        className={clsx(
                                                            classes.image,
                                                            'parallax',
                                                            'parallax-3'
                                                        )}
                                                        src={image2}
                                                        alt=""
                                                        data-speed={1}
                                                        style={{
                                                            height: '120%',
                                                        }}
                                                    />
                                                </div>
                                            </div>
                                        </Box>

                                        <Box
                                            px={5}
                                            py={3}
                                            pr={0}
                                            textAlign="right"
                                        >
                                            <Typography
                                                display="block"
                                                variant="caption"
                                                gutterBottom
                                            >
                                                {/* 編輯 - 文字 */}
                                                Decathlon recruitment.
                                            </Typography>
                                            <Typography
                                                display="block"
                                                variant="caption"
                                                color="textSecondary"
                                            >
                                                {/* 編輯 - 文字 */}
                                                <i>Web & mobile - 2020</i>
                                            </Typography>
                                            <br />
                                            <Link className={classes.linkSmall}>
                                                {/* 編輯 - 文字 */}
                                                See the project
                                            </Link>
                                        </Box>
                                    </div>
                                </div>
                            </Grid>
                            <Grid item xs={12} md={7}></Grid>
                        </Grid>
                    </Box>
                </div>
                {/*      Wrap 4      */}
                <div id="wrap4">
                    <Box px={5} pb={8} mt={{ sm: -60 }}>
                        <Grid container spacing={4} justify="center">
                            <Grid
                                item
                                xs={12}
                                sm={1}
                                style={{ paddingTop: 0, paddingBottom: 0 }}
                            ></Grid>
                            <Grid item xs={12} sm={11}>
                                <Box
                                    mb={4}
                                    color={theme.palette.secondary.main}
                                >
                                    <div
                                        className={clsx(
                                            classes.wave,
                                            'wrap4-wave'
                                        )}
                                    >
                                        <WaveIcon width={48} />
                                    </div>
                                </Box>
                                <Box>
                                    <div className="overflow-hidden">
                                        <Typography
                                            className="wrap4-line"
                                            variant={matchSm ? 'h1' : 'h2'}
                                            color="secondary"
                                        >
                                            {/* 編輯 - 文字 */}
                                            ID
                                        </Typography>
                                    </div>

                                    <div className="overflow-hidden">
                                        <Typography
                                            className="wrap4-line"
                                            variant={matchSm ? 'h1' : 'h2'}
                                            color="secondary"
                                            style={{ marginTop: -16 }}
                                        >
                                            {/* 編輯 - 文字 */}
                                            EAS.
                                        </Typography>
                                    </div>
                                </Box>
                            </Grid>

                            <Box
                                position="relative"
                                zIndex={-1}
                                mx={-5}
                                mb={5}
                                mt={-8}
                                px={matchSm ? 5 : 0}
                                overflow="hidden"
                                width="100vw"
                                maxHeight={matchSm ? 240 : undefined}
                            >
                                <div
                                    className="overflow-hidden"
                                    style={{
                                        height: 0,
                                        width: '100%',
                                        paddingTop: matchSm ? '22.5%' : '50%',
                                        position: 'relative',
                                    }}
                                >
                                    <div
                                        className="absolute-center"
                                        style={{
                                            width: '100%',
                                            height: '100%',
                                        }}
                                    >
                                        <img
                                            className={clsx(
                                                classes.image,
                                                'parallax',
                                                'parallax-4'
                                            )}
                                            src={image3}
                                            alt=""
                                            data-speed={1}
                                            style={{
                                                height: '120%',
                                            }}
                                        />
                                    </div>
                                </div>
                            </Box>

                            <Grid item xs={12} sm={10}>
                                <Grid container spacing={matchMd ? 6 : 3}>
                                    <Grid item xs={12} sm={8} md={4}>
                                        <Box mb={1}>
                                            <div className="overflow-hidden">
                                                <Typography
                                                    className="wrap4-line"
                                                    variant={
                                                        matchSm
                                                            ? 'h4'
                                                            : 'subtitle1'
                                                    }
                                                >
                                                    {/* 編輯 - 文字 */}A studio
                                                    dedicated to
                                                </Typography>
                                            </div>

                                            <div className="overflow-hidden">
                                                <Typography
                                                    className="wrap4-line"
                                                    variant={
                                                        matchSm
                                                            ? 'h4'
                                                            : 'subtitle1'
                                                    }
                                                >
                                                    <i>
                                                        {/* 編輯 - 文字 */}
                                                        startups
                                                        <span
                                                            style={{
                                                                color:
                                                                    theme
                                                                        .palette
                                                                        .secondary
                                                                        .main,
                                                            }}
                                                        >
                                                            .
                                                        </span>
                                                    </i>
                                                </Typography>
                                            </div>
                                        </Box>
                                    </Grid>

                                    <Grid item xs={12} sm={8} md={4}>
                                        <Box color={theme.palette.grey[900]}>
                                            <Typography
                                                className="wrap4-text"
                                                variant="body2"
                                                color="inherit"
                                            >
                                                {/* 編輯 - 文字 */}
                                                Have you dreamed up an
                                                innovative new digital platform,
                                                a revolutionary mobile
                                                application, a life-enhancing
                                                chatbot, or do you just want to
                                                change the world? Let's do it
                                                together.
                                            </Typography>
                                        </Box>
                                    </Grid>
                                    <Grid item xs={12} sm={8} md={4}>
                                        <div className="wrap4-text">
                                            <Box
                                                mb={3}
                                                color={theme.palette.grey[900]}
                                            >
                                                <Typography
                                                    variant="body2"
                                                    color="inherit"
                                                >
                                                    {/* 編輯 - 文字 */}
                                                    We're here to make your
                                                    dreams come true. Our
                                                    support method will turn
                                                    your bright idea into a real
                                                    prototype, and then grow it
                                                    into a profitable business.
                                                </Typography>
                                            </Box>
                                            {/* 編輯 - 文字 */}
                                            <Link>See how</Link>
                                        </div>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Box>
                </div>{' '}
            </div>

            {/*      Links      */}
            {matchSm && (
                <div
                    className="absolute-vertical"
                    style={{
                        position: 'fixed',
                        zIndex: theme.zIndex.tooltip,
                        width: 24,
                        right: 8,
                        color: theme.palette.text.primary,
                    }}
                >
                    {renderLinks()}
                </div>
            )}

            {/*      Footer      */}
            <div className={classes.rooter} ref={rooterRef}>
                <Box
                    px={5}
                    py={8}
                    style={{ backgroundColor: theme.palette.common.white }}
                >
                    <Grid container spacing={4}>
                        <Grid
                            item
                            xs={12}
                            sm={1}
                            style={{ paddingTop: 0, paddingBottom: 0 }}
                        ></Grid>
                        <Grid item xs={12} sm={11}>
                            <Typography variant="caption" display="block">
                                {/* 編輯 - 文字 */}
                                <i>Want to work with us ?</i>
                            </Typography>
                            {/* 編輯 - 信箱地址 */}
                            <Typography
                                variant="h5"
                                component="a"
                                href="mailto:contact@wokine.com?subject=Nouveau%20briefing"
                                color="textPrimary"
                                style={{
                                    textTransform: 'uppercase',
                                    fontWeight: 900,
                                }}
                            >
                                {/* 編輯 - 文字 */}
                                Brief us
                            </Typography>

                            {!matchSm && (
                                <Box mt={2} color={theme.palette.grey[500]}>
                                    {renderLinks()}
                                </Box>
                            )}
                        </Grid>
                    </Grid>
                </Box>
            </div>
        </Container>
    )
}

export default Home

function wrapLines(target: string, lineClass: string, parentId: string) {
    if (parentId && !document.getElementById(parentId)) {
        const results = Splitting({
            target,
            by: 'lines',
        })

        _.forEach(results, (el) => {
            const parentNode = _.get(el, 'lines.0.0.parentNode') as HTMLElement

            if (parentNode && el.lines) {
                const tracked = document.createElement('span')
                tracked.id = parentId
                parentNode.appendChild(tracked)

                _.forEach(el.lines, (words) => {
                    const parent = document.createElement('span')
                    parent.classList.add('d-block', 'overflow-hidden')
                    parentNode.appendChild(parent)

                    const line = document.createElement('span')
                    line.classList.add(lineClass, 'd-block')
                    parent.appendChild(line)
                    _.forEach(words, (word, index) => {
                        line.appendChild(word)

                        if (index + 1 !== words.length) {
                            const space = document.createElement('span')
                            space.innerHTML = ' '
                            line.appendChild(space)
                        }
                    })
                })
            }
        })
    }
}
