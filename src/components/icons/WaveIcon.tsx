import React from 'react'

type Props = {
    className: string
    color: string
    width: number
    [k: string]: any
}

export default function WaveIcon(props: Partial<Props>) {
    const { className = '', color = 'currentColor', width = 16, ...rest } =
        props || {}

    return (
        <svg
            className={className}
            width={width}
            height={(width / 4).toFixed()}
            viewBox="0 0 16 4"
            version="1.1"
            xmlns="http://www.w3.org/2000/svg"
            {...rest}
        >
            <title>icon/wave</title>
            <g
                transform="translate(-1.000000, 0.000000)"
                fill={color}
                fillRule="nonzero"
            >
                <polygon points="15 2.293 17.000187 0.29270618 16.999 1.707 15 3.70710678 13 1.707 11 3.70710678 9 1.707 7 3.70710678 5 1.707 3 3.70710678 1 1.707 1 0.292893219 3 2.293 5 0.292893219 7 2.293 9 0.292893219 11 2.293 13 0.292893219"></polygon>
            </g>
        </svg>
    )
}
